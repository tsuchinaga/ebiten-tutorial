package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
	"math"
)

var (
	ebitenImage *ebiten.Image
)

func update(screen *ebiten.Image) error {
	if ebiten.IsRunningSlowly() {
		return nil
	}

	// 45度時計回りに回転。
	// π/4で45度が表せる
	op := new(ebiten.DrawImageOptions)
	op.GeoM.Rotate(math.Pi / 4)
	_ = screen.DrawImage(ebitenImage, op)

	return nil
}

func main() {
	var err error
	ebitenImage, err = ebitenutil.NewImageFromURL("https://raw.githubusercontent.com/hajimehoshi/ebiten-book-code/master/ja/ebiten.png")
	if err != nil {
		log.Fatal(err)
	}

	if err := ebiten.Run(update, 320, 240, 2, "Image Rotating"); err != nil {
		log.Fatal(err)
	}
}
