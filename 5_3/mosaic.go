package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
)

var (
	gopherImage *ebiten.Image
	tmpImage    *ebiten.Image
)

func update(screen *ebiten.Image) error {
	if ebiten.IsRunningSlowly() {
		return nil
	}

	// 元の画像を等倍で表示
	_ = screen.DrawImage(gopherImage, nil)

	// 1/4のサイズにしたものを4倍にして表示
	const scale = 4.0
	_ = tmpImage.Clear()
	op := new(ebiten.DrawImageOptions)
	op.GeoM.Scale(1/scale, 1/scale)
	_ = tmpImage.DrawImage(gopherImage, op)

	op = new(ebiten.DrawImageOptions)
	op.GeoM.Scale(scale, scale)
	op.GeoM.Translate(100, 0)
	_ = screen.DrawImage(tmpImage, op)

	return nil
}

func main() {
	var err error

	gopherImage, _, err = ebitenutil.NewImageFromFile("./5_3/gopher.png", ebiten.FilterNearest)
	if err != nil {
		log.Fatal(err)
	}

	// オフスクリーンバッファ
	// 大きさはImageの大きさ以上であればいいとのこと。
	// チュートリアルではスクリーンイメージと同じサイズにしていましたが、せっかくなのでgopherImageと同じ大きさにしてみる
	// モザイク化のために、オフスクリーンバッファにはNearestのフィルターをかける
	w, h := gopherImage.Size()
	tmpImage, err = ebiten.NewImage(w, h, ebiten.FilterNearest)
	if err != nil {
		log.Fatal(err)
	}

	if err = ebiten.Run(update, 320, 240, 2, "Mosaic"); err != nil {
		log.Fatal(err)
	}
}
