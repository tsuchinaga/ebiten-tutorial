package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
)

var (
	ebitenImageNearest *ebiten.Image
	ebitenImageLinear  *ebiten.Image
)

func update(screen *ebiten.Image) error {
	if ebiten.IsRunningSlowly() {
		return nil
	}

	op := new(ebiten.DrawImageOptions)
	op.GeoM.Scale(4, 4)
	_ = screen.DrawImage(ebitenImageNearest, op)

	op = new(ebiten.DrawImageOptions)
	op.GeoM.Scale(4, 4)
	op.GeoM.Translate(0, 100)
	_ = screen.DrawImage(ebitenImageLinear, op)

	return nil
}

func main() {
	var err error
	ebitenImageNearest, _, err = ebitenutil.NewImageFromFile("./4_8/ebiten.png", ebiten.FilterNearest)
	if err != nil {
		log.Fatal(err)
	}
	ebitenImageLinear, _, err = ebitenutil.NewImageFromFile("./4_8/ebiten.png", ebiten.FilterLinear)
	if err != nil {
		log.Fatal(err)
	}

	if err := ebiten.Run(update, 320, 240, 2, "Image Filters"); err != nil {
		log.Fatal(err)
	}
}
