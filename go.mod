module gitlab.com/tsuchinaga/ebiten-tutorial

require (
	github.com/go-gl/gl v0.0.0-20181026044259-55b76b7df9d2 // indirect
	github.com/go-gl/glfw v0.0.0-20181213070059-819e8ce5125f // indirect
	github.com/gopherjs/gopherjs v0.0.0-20181103185306-d547d1d9531e // indirect
	github.com/hajimehoshi/ebiten v1.8.1 // indirect
	github.com/hajimehoshi/oto v0.2.2 // indirect
	github.com/kr/pty v1.1.3 // indirect
	github.com/theckman/go-flock v0.7.0 // indirect
	golang.org/x/exp v0.0.0-20190123073158-f1c91bc264ca // indirect
	golang.org/x/image v0.0.0-20190118043309-183bebdce1b2 // indirect
	golang.org/x/mobile v0.0.0-20190107162257-dc0771356504 // indirect
	golang.org/x/sys v0.0.0-20190123074212-c6b37f3e9285 // indirect
	golang.org/x/tools v0.0.0-20190124004107-78ee07aa9465 // indirect
)
