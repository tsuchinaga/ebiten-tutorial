package main

import (
	"github.com/hajimehoshi/ebiten"
	"image/color"
	"log"
)

func update(screen *ebiten.Image) error {
	if ebiten.IsRunningSlowly() {
		return nil
	}

	// 画面を赤色で塗りつぶす
	// color.RGBAは8bitカラーを表す
	// Fillもまたerrorは常にnil
	_ = screen.Fill(color.RGBA{R: 255, G: 0, B: 0, A: 255})
	return nil
}

func main() {
	if err := ebiten.Run(update, 320, 240, 2, "Fill"); err != nil {
		log.Fatal(err)
	}
}
