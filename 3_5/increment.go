package main

import (
	"fmt"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
)

// カウンタ
var i = 0

func update(screen *ebiten.Image) error {
	i++

	if ebiten.IsRunningSlowly() {
		return nil
	}

	// カウンタを表示
	msg := fmt.Sprintf("%d", i)
	_ = ebitenutil.DebugPrint(screen, msg)

	return nil
}

func main() {
	if err := ebiten.Run(update, 320, 240, 2, "Increment"); err != nil {
		log.Fatal(err)
	}
}
