package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
	"math"
)

var (
	ebitenImage *ebiten.Image
)

func update(screen *ebiten.Image) error {
	if ebiten.IsRunningSlowly() {
		return nil
	}

	_, h := ebitenImage.Size()

	for i, a := range []int{0, 60, 120, 180, 240, 300} {
		op := new(ebiten.DrawImageOptions)
		op.GeoM.Translate(0, float64(i*h)) // 横並びだとはみ出るので、縦並びにして見やすくしてみた
		op.ColorM.RotateHue(float64(a) * math.Pi / 180)
		_ = screen.DrawImage(ebitenImage, op)
	}

	return nil
}

func main() {
	var err error
	ebitenImage, err = ebitenutil.NewImageFromURL("https://raw.githubusercontent.com/hajimehoshi/ebiten-book-code/master/ja/ebiten.png")
	if err != nil {
		log.Fatal(err)
	}

	if err := ebiten.Run(update, 320, 240, 2, "Hue"); err != nil {
		log.Fatal(err)
	}
}
