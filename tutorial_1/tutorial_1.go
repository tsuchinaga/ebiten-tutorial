package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
)

func update(screen *ebiten.Image) error {
	_ = ebitenutil.DebugPrint(screen, "Our first game in Ebiten!")
	return nil
}

func main() {
	if err := ebiten.Run(update, 320, 240, 2, "Hello, World!"); err != nil {
		log.Fatal(err)
	}
}
