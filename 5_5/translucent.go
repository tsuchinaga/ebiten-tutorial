package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
)

var (
	ebitenImage *ebiten.Image
)

func update(screen *ebiten.Image) error {
	if ebiten.IsRunningSlowly() {
		return nil
	}

	// 色行列を使用して、アルファ値だけを半分にする
	op := new(ebiten.DrawImageOptions)
	op.ColorM.Scale(1, 1, 1, 0.5)
	_ = screen.DrawImage(ebitenImage, op)

	return nil
}

func main() {
	var err error
	ebitenImage, err = ebitenutil.NewImageFromURL("https://raw.githubusercontent.com/hajimehoshi/ebiten-book-code/master/ja/ebiten.png")
	if err != nil {
		log.Fatal(err)
	}

	if err := ebiten.Run(update, 320, 240, 2, "Translucent"); err != nil {
		log.Fatal(err)
	}
}
