package main

import (
	"github.com/hajimehoshi/ebiten"
	"log"
)

func update(_ *ebiten.Image) error {
	// 状態更新処理
	if ebiten.IsRunningSlowly() {

		// IsRunningSlowlyがtrueのときは、screenに対する描画結果は実際の画面に反映されない
		log.Println("描画処理スキップ")
		return nil
	}

	// 描画処理
	return nil
}

func main() {
	if err := ebiten.Run(update, 320, 240, 2, "Test"); err != nil {
		log.Fatal(err)
	}
}
