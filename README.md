# ebiten-tutorial

Golangのゲーム開発ライブラリのEbitenのチュートリアルを実際に行なった結果を入れているだけのリポジトリ。

## チュートリアル

https://drive.google.com/file/d/1od5nugweu4U6pwpW_8Q8nyNJLPRayWfO/view

## 内容

* **3.1 最初のEbitenプログラム** PDF32ページ  
    ただウィンドウを開くだけのチュートリアル
* **3.2 FPS** PDF34ページ  
    1/60秒で処理が終わらず描画するとカクツクことが分かっている場合、簡単にスキップすることが出来る
* **3.3 Hello, World!** PDF36ページ  
    デバッグ用機能を使って、画面に文字を出力する
* **3.4 ブラウザでの実行** PDF38ページ  
    jsファイルの書き出し。Windowsの場合、gopherjsのコマンドの先頭に`GOOS=linux`が必要  
    `GOOS=linux gopherjs build -o 3_4/helloworld.js 3_3/helloworld.go`
* **3.5 更新** PDF39ページ  
    まだデバッグ機能を使ってるけど、画面の数字が変わるようになった
* **4.2 塗りつぶし** PDF43ページ  
    まっかっか
* **4.3 画像** PFD45ページ  
    画像はここにありました: https://raw.githubusercontent.com/hajimehoshi/ebiten-book-code/master/ja/ebiten.png  
    URLからも直接ebiten.Imageを作れるので便利です
* **4.6.1 描画位置 - 固定位置** PDF48ページ
* **4.6.2 描画位置 - 移動** PDF58ページ
* **4.7.1 拡大・縮小 - 単純な拡大** PDF60ページ
* **4.7.2 拡大・縮小 - 平行移動と拡大** PDF61ページ  
    文面からだけだとどのように違うのかのイメージが難しかったけど、スケールは常に原点からだと考えれば比較的分かりやすい気がした
* **4.8 フィルタ** PDF65ページ  
    フィルタを指定するにはURLからではなくパスからファイルを読む必要があるみたい
* **4.9.1 回転 - 単純な回転** PDF67ページ  
    回転するが原点中心で回転するため、思ってたのと違うという感じ
* **4.9.2 回転 - 回転中心** PDF69ページ  
    やってることは単純で、画像の中心を原点に重ねてから回転させて、元の位置に戻してるだけ
* **5.1 オフスクリーンレンダリング** PDF72ページ
* **5.2 オフスクリーンバッファの使いまわし** PDF76ページ  
    Imageのクリアをしても、そこまででscreen等に描画したものは書き換わらずに登録された時点の状態を保持しているという特性
* **5.3 モザイク** PDF79ページ  
    チュートリアルのコードが分かりにくかったので、少し順番をかえてます
* **5.5 半透明** PDF84ページ  
    色変換を行列でやるから最初はピンとこないけど、何も変わらない単位行列のうち、アルファ値だけを0.5(半分)にしてると思うと分かりやすいかも？
* **5.6 モノクロ** PDF86ページ  
    HSVは色相、彩度、明度での色の表現。彩度を殺して、明度を中間にしてグレースケールにしてる。
* **5.7 色相** PDF89ページ
* **5.8 セピア** PDF91ページ  
    画像はここにあった: https://raw.githubusercontent.com/hajimehoshi/ebiten-book-code/master/ja/gophers_photo.jpg  
    が、jpgだとフォーマットエラーになるようなので、適当なpngでテスト  
    追記: importに`_ "image/jpeg"`を追加したら動いた。どうもデコーダを指定しなくてはいけないみたい
* **5.9 部分描画** PDF94ページ  
    interfaceを実装してたらそれを読み取ってくれるの面白い
* **5.10 大量スプライト** PDF98ページ  
    100個のエビテンを一瞬で描画するんですからすごいですよねー。ちょっといじって毎秒エビテンの位置が変わるようにしています
* **5.11 ラスタスクロール** PDF101ページ  
    理屈はわからないでもないけど、いきなりSinとかでると思考停止しそうになる
* **6.1.1 キーボード - 入力テスト** PDF106ページ
* **6.1.2 キーボード - トリガー** PDF108ページ  
    どれくらい長く押されたかみたいなのを取って表示するように変更してみてる
* **6.2 マウス** PDF110ページ
* **tutorial.1 Your first game in Ebiten**  
    https://github.com/hajimehoshi/ebiten/wiki/Tutorial%3AYour-first-game-in-Ebiten
* **tutorial.2 Screen, colors and squares**  
    https://github.com/hajimehoshi/ebiten/wiki/Tutorial%3AScreen%2C-colors-and-squares
* **tutorial.3 Handle user inputs**  
    https://github.com/hajimehoshi/ebiten/wiki/Tutorial%3AHandle-user-inputs
* **issue.3 テキストの表示**  
    https://gitlab.com/tsuchinaga/ebiten-tutorial/issues/3
