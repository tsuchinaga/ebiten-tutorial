package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
	"math"
)

var (
	ebitenImage    *ebiten.Image
	offscreenImage *ebiten.Image
	angle          int
)

func update(screen *ebiten.Image) error {
	angle += 1
	angle %= 360

	if ebiten.IsRunningSlowly() {
		return nil
	}

	// オフスクリーンバッファというか、自分で明示的に生成したimageは自動でクリアされないのでここでクリア。
	_ = offscreenImage.Clear()

	// オフスクリーンバッファにエビテンを2つ表示
	_ = offscreenImage.DrawImage(ebitenImage, nil)

	w, h := ebitenImage.Size()
	op := new(ebiten.DrawImageOptions)
	op.GeoM.Translate(-float64(w)/2, -float64(h)/2)
	op.GeoM.Rotate(float64(angle) * math.Pi / 180)
	op.GeoM.Translate(float64(w)/2+float64(w)/2, float64(h)/2) // 元に戻すのと、追加でずらすのの合計
	_ = offscreenImage.DrawImage(ebitenImage, op)

	// オフスクリーンバッファを画面に描画
	_ = screen.DrawImage(offscreenImage, nil)

	// 同じものを拡大して表示
	op = new(ebiten.DrawImageOptions)
	op.GeoM.Scale(2, 2)
	op.GeoM.Translate(100, 100)
	_ = screen.DrawImage(offscreenImage, op)
	return nil
}

func main() {
	var err error
	ebitenImage, err = ebitenutil.NewImageFromURL("https://raw.githubusercontent.com/hajimehoshi/ebiten-book-code/master/ja/ebiten.png")
	if err != nil {
		log.Fatal(err)
	}

	// オフスクリーンレンダリング用のバッファイメージ。
	// ebiten.Imageを作るのは重たい処理らしいので、mainで一発作っておくとかが望ましい
	offscreenImage, err = ebiten.NewImage(100, 100, ebiten.FilterNearest)
	if err != nil {
		log.Fatal(err)
	}

	if err := ebiten.Run(update, 320, 240, 2, "Offscreen"); err != nil {
		log.Fatal(err)
	}
}
