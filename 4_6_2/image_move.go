package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
)

var (
	ebitenImage  *ebiten.Image
	ebitenImageX float64
)

func update(screen *ebiten.Image) error {
	if ebiten.IsRunningSlowly() {
		return nil
	}

	// 描画先のX座標を更新する。
	ebitenImageX += 1
	op := new(ebiten.DrawImageOptions)
	op.GeoM.Translate(ebitenImageX, 10)
	_ = screen.DrawImage(ebitenImage, op)

	return nil
}

func main() {
	// ebitenutilパッケージのNewImageFromFile関数を使って、ファイル名から直接*ebiten.Imageを作成する。
	// 2つめの戻り値は*image.Imageだが、今回は使わないので無視する。
	// フィルタとして今回はFilterNearestを使用した。
	// この値は画像の拡大縮小の際のフィルタを表すが、実際に画像を拡大縮小したりしない限りは使用されないので、今回はとりあえず何でも良い。
	var err error
	// ebitenImage, _, err = ebitenutil.NewImageFromFile("./4_3/ebiten.png", ebiten.FilterNearest)
	ebitenImage, err = ebitenutil.NewImageFromURL("https://raw.githubusercontent.com/hajimehoshi/ebiten-book-code/master/ja/ebiten.png")
	if err != nil {
		log.Fatal(err)
	}

	if err := ebiten.Run(update, 320, 240, 2, "Image Move"); err != nil {
		log.Fatal(err)
	}
}
