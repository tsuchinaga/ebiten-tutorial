package main

import (
	"github.com/golang/freetype/truetype"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/text"
	"golang.org/x/image/font"
	"image/color"
	"io/ioutil"
	"log"
)

const (
	dpi          = 96
	screenWidth  = 320
	screenHeight = 240
)

var (
	migmix font.Face
)

func init() {
	fileByte, err := ioutil.ReadFile("./resources/migmix-1m-regular.ttf")
	if err != nil {
		log.Fatal(err)
	}
	tt, err := truetype.Parse(fileByte)
	if err != nil {
		log.Fatal(err)
	}

	migmix = truetype.NewFace(tt, &truetype.Options{
		Size: 12, DPI: dpi, Hinting: font.HintingFull,
	})
}

func textWidth(face font.Face, str string) int {
	b, _ := font.BoundString(face, str)
	return (b.Max.X - b.Min.X).Ceil()
}

func update(screen *ebiten.Image) error {

	var msg string
	msg = "こんにちわーるど(Left)"
	text.Draw(screen, msg, migmix, 0, 40, color.White)

	msg = "こんにちわーるど(Center)"
	text.Draw(screen, msg, migmix, (screenWidth-textWidth(migmix, msg))/2, 80, color.RGBA{R: 255, A: 255})

	msg = "こんにちわーるど(Right)"
	text.Draw(screen, msg, migmix, screenWidth-textWidth(migmix, msg), 120, color.RGBA{B: 255, A: 255})

	return nil
}

func main() {
	if err := ebiten.Run(update, screenWidth, screenHeight, 2, "Show Text"); err != nil {
		log.Fatal(err)
	}
}
