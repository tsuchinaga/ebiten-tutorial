package main

import (
	"fmt"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
)

var (
	keyAState int
)

func update(screen *ebiten.Image) error {
	if ebiten.IsKeyPressed(ebiten.KeyA) {
		keyAState += 1
	} else {
		keyAState = 0
	}

	if ebiten.IsRunningSlowly() {
		return nil
	}

	// if keyAState == 1 {
	if keyAState > 0 {
		_ = ebitenutil.DebugPrint(screen, fmt.Sprintf("A key is triggered!(%d fps)", keyAState))
	}

	return nil
}

func main() {
	if err := ebiten.Run(update, 320, 240, 2, "Key Trigger"); err != nil {
		log.Fatal(err)
	}
}
