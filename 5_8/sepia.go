package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	_ "image/jpeg"
	"log"
)

var (
	tick        int
	gopherImage *ebiten.Image
)

func update(screen *ebiten.Image) error {
	tick += 1
	tick %= 120

	if ebiten.IsRunningSlowly() {
		return nil
	}

	op := new(ebiten.DrawImageOptions)
	if tick >= 60 { // 2秒のうち1秒は変換した画像を表示する
		op.ColorM.ChangeHSV(0, 5.0/15.0, 1)
		op.ColorM.Translate(2.0/15.0, -2.0/15.0, -4.0/15.0, 0)
	}
	_ = screen.DrawImage(gopherImage, op)
	return nil
}

func main() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)

	var err error
	gopherImage, err = ebitenutil.NewImageFromURL("https://raw.githubusercontent.com/hajimehoshi/ebiten-book-code/master/ja/gophers_photo.jpg")
	if err != nil {
		log.Fatal(err)
	}

	if err := ebiten.Run(update, 320, 240, 2, "Sepia"); err != nil {
		log.Fatal(err)
	}
}
