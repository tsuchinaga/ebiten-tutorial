package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
	"math"
)

var (
	ebitenImage    *ebiten.Image
	offscreenImage *ebiten.Image
	angle          int
)

func update(screen *ebiten.Image) error {
	angle += 1
	angle %= 360

	if ebiten.IsRunningSlowly() {
		return nil
	}

	_ = offscreenImage.Clear()
	_ = offscreenImage.DrawImage(ebitenImage, nil)
	_ = screen.DrawImage(offscreenImage, nil)

	// オフスクリーンバッファを再度初期化
	// 直前に登録したオフスクリーンはその時点のものが反映されているため、ここでクリアしてもここまでの内容には影響しない
	_ = offscreenImage.Clear()
	w, h := ebitenImage.Size()
	op := new(ebiten.DrawImageOptions)
	op.GeoM.Translate(-float64(w)/2, -float64(h)/2)
	op.GeoM.Rotate(float64(angle) * math.Pi / 180)
	op.GeoM.Translate(float64(w)/2+float64(w)/2, float64(h)/2) // 元に戻すのと、追加でずらすのの合計
	_ = offscreenImage.DrawImage(ebitenImage, op)

	op = new(ebiten.DrawImageOptions)
	op.GeoM.Translate(50, 0)
	_ = screen.DrawImage(offscreenImage, op)
	return nil
}

func main() {
	var err error
	ebitenImage, err = ebitenutil.NewImageFromURL("https://raw.githubusercontent.com/hajimehoshi/ebiten-book-code/master/ja/ebiten.png")
	if err != nil {
		log.Fatal(err)
	}

	offscreenImage, err = ebiten.NewImage(100, 100, ebiten.FilterNearest)
	if err != nil {
		log.Fatal(err)
	}

	if err := ebiten.Run(update, 320, 240, 2, "Reusing Offscreen"); err != nil {
		log.Fatal(err)
	}
}
