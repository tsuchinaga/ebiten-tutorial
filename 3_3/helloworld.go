package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
)

func update(screen *ebiten.Image) error {
	if ebiten.IsRunningSlowly() {
		return nil
	}

	// デバッグ目的で使うものですが、文字を書くための関数がある
	// errorを返すけど、常にnilが返されるから捨ててしまう
	_ = ebitenutil.DebugPrint(screen, "Hello, World!")
	return nil
}

func main() {
	if err := ebiten.Run(update, 320, 240, 2, "Hello"); err != nil {
		log.Fatal(err)
	}
}
