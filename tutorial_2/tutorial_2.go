package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"image/color"
	"log"
)

var square *ebiten.Image

func init() {
	square, _ = ebiten.NewImage(16, 16, ebiten.FilterNearest)
	_ = square.Fill(color.White)
}

func update(screen *ebiten.Image) error {
	_ = screen.Fill(color.NRGBA{R: 0xff, A: 0xff})

	_ = ebitenutil.DebugPrint(screen, "Our first game in Ebiten!")

	op := new(ebiten.DrawImageOptions)
	op.GeoM.Translate(64, 64)
	_ = screen.DrawImage(square, op)
	return nil
}

func main() {
	if err := ebiten.Run(update, 320, 240, 2, "Hello, World!"); err != nil {
		log.Fatal(err)
	}
}
