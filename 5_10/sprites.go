package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	_ "image/jpeg"
	"log"
	"math/rand"
	"time"
)

var (
	ebitenImage *ebiten.Image
	positions   []position
	count       int
)

// 座標を表す構造体
type position struct {
	x, y int
}

type sprites struct {
	ebiten.ImageParts
}

func (s *sprites) Len() int {
	return len(positions)
}

// 描画元の矩形の範囲を返す関数
func (s *sprites) Src(i int) (int, int, int, int) {
	w, h := ebitenImage.Size()
	return 0, 0, w, h
}

// 描画先の矩形の範囲を返す関数
func (s *sprites) Dst(i int) (int, int, int, int) {
	w, h := ebitenImage.Size()
	return positions[i].x, positions[i].y, positions[i].x + w, positions[i].y + h
}

func init() {
	setPositions()
}

func setPositions() {
	positions = make([]position, 100)

	rand.Seed(time.Now().UnixNano())
	for i := range positions {
		positions[i].x = rand.Intn(320)
		positions[i].y = rand.Intn(240)
	}
}

func update(screen *ebiten.Image) error {
	count += 1

	if ebiten.IsRunningSlowly() {
		return nil
	}

	if count > 60 {
		count = 0
		setPositions()
	}

	op := new(ebiten.DrawImageOptions)
	op.ImageParts = new(sprites)

	_ = screen.DrawImage(ebitenImage, op)
	return nil
}

func main() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)

	var err error
	ebitenImage, err = ebitenutil.NewImageFromURL("https://raw.githubusercontent.com/hajimehoshi/ebiten-book-code/master/ja/ebiten.png")
	if err != nil {
		log.Fatal(err)
	}

	if err := ebiten.Run(update, 320, 240, 2, "Parts"); err != nil {
		log.Fatal(err)
	}
}
