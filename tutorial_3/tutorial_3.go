package main

import (
	"fmt"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
)

func update(screen *ebiten.Image) error {
	var msg string

	// マウス入力
	x, y := ebiten.CursorPosition()
	msg += fmt.Sprintf("X: %d, Y: %d\n", x, y)

	if ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
		msg += "You're pressing the 'Left' mouse button.\n"
	}
	if ebiten.IsMouseButtonPressed(ebiten.MouseButtonRight) {
		msg += "You're pressing the 'Right' mouse button.\n"
	}
	if ebiten.IsMouseButtonPressed(ebiten.MouseButtonMiddle) {
		msg += "You're pressing the 'Middle' mouse button.\n"
	}

	// キー入力
	if ebiten.IsKeyPressed(ebiten.KeyUp) {
		msg += "You're pressing the 'Up' button.\n"
	}
	if ebiten.IsKeyPressed(ebiten.KeyDown) {
		msg += "You're pressing the 'Down' button.\n"
	}
	if ebiten.IsKeyPressed(ebiten.KeyLeft) {
		msg += "You're pressing the 'Left' button.\n"
	}
	if ebiten.IsKeyPressed(ebiten.KeyRight) {
		msg += "You're pressing the 'Right' button.\n"
	}

	_ = ebitenutil.DebugPrint(screen, msg)
	return nil
}

func main() {
	if err := ebiten.Run(update, 320, 240, 2, "Hello world!"); err != nil {
		log.Fatal(err)
	}
}
