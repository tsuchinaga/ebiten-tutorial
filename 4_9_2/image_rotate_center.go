package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
	"math"
)

var (
	ebitenImage *ebiten.Image
)

func update(screen *ebiten.Image) error {
	if ebiten.IsRunningSlowly() {
		return nil
	}

	// 画像の大きさを得る
	w, h := ebitenImage.Size()

	// ebitenImageを時計回りの向きに、画像の中心を回転中心として45度回転させる
	op := new(ebiten.DrawImageOptions)
	op.GeoM.Translate(-float64(w)/2, -float64(h)/2)
	op.GeoM.Rotate(math.Pi / 4)
	op.GeoM.Translate(float64(w)/2, float64(h)/2)
	_ = screen.DrawImage(ebitenImage, op)

	return nil
}

func main() {
	var err error
	ebitenImage, err = ebitenutil.NewImageFromURL("https://raw.githubusercontent.com/hajimehoshi/ebiten-book-code/master/ja/ebiten.png")
	if err != nil {
		log.Fatal(err)
	}

	if err := ebiten.Run(update, 320, 240, 2, "Image Rotating Center"); err != nil {
		log.Fatal(err)
	}
}
