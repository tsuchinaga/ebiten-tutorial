package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
)

var (
	ebitenImage *ebiten.Image
)

func update(screen *ebiten.Image) error {
	if ebiten.IsRunningSlowly() {
		return nil
	}

	// 色行列を使用して、アルファ値だけを半分にする
	op := new(ebiten.DrawImageOptions)
	op.ColorM.ChangeHSV(0, 0, 1)
	_ = screen.DrawImage(ebitenImage, op)

	return nil
}

func main() {
	var err error
	ebitenImage, err = ebitenutil.NewImageFromURL("https://raw.githubusercontent.com/hajimehoshi/ebiten-book-code/master/ja/ebiten.png")
	if err != nil {
		log.Fatal(err)
	}

	if err := ebiten.Run(update, 320, 240, 2, "Monochrome"); err != nil {
		log.Fatal(err)
	}
}
