package main

import (
	"fmt"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
)

func update(screen *ebiten.Image) error {
	x, y := ebiten.CursorPosition()
	msg := fmt.Sprintf("(%d, %d)\n", x, y)

	if ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
		msg += "Left button is pressed!"
	}

	if ebiten.IsRunningSlowly() {
		return nil
	}

	_ = ebitenutil.DebugPrint(screen, msg)

	return nil
}

func main() {
	if err := ebiten.Run(update, 320, 240, 2, "Mouse"); err != nil {
		log.Fatal(err)
	}
}
