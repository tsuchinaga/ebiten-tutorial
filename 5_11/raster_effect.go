package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	_ "image/jpeg"
	"log"
	"math"
)

var (
	gopherImage *ebiten.Image
	parts       *rasterEffectParts
)

type rasterEffectParts struct {
	ebiten.ImageParts
	image *ebiten.Image
	angle int
}

func (r *rasterEffectParts) Update() {
	r.angle += 1
	r.angle %= 360
}

// 描画するパースの数を返す関数
func (r *rasterEffectParts) Len() int {
	_, h := r.image.Size()
	return h
}

// 描画元の矩形の範囲を返す関数
func (r *rasterEffectParts) Src(i int) (int, int, int, int) {
	w, _ := r.image.Size()
	return 0, i, w, i + 1
}

// 描画先の矩形の範囲を返す関数
func (r *rasterEffectParts) Dst(i int) (int, int, int, int) {
	const amplitude = 8
	a := r.angle + 4*i
	d := int(math.Floor(math.Sin(float64(a)*math.Pi/180) * amplitude))

	w, _ := r.image.Size()

	return d, i, w + d, i + 1
}

func update(screen *ebiten.Image) error {
	parts.Update()

	if ebiten.IsRunningSlowly() {
		return nil
	}

	op := new(ebiten.DrawImageOptions)
	op.ImageParts = parts
	_ = screen.DrawImage(gopherImage, op)

	return nil
}

func main() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)

	var err error
	gopherImage, err = ebitenutil.NewImageFromURL("https://raw.githubusercontent.com/hajimehoshi/ebiten-book-code/master/ja/gophers_photo.jpg")
	if err != nil {
		log.Fatal(err)
	}

	parts = &rasterEffectParts{
		image: gopherImage,
	}

	if err := ebiten.Run(update, 320, 240, 2, "Raster Effect"); err != nil {
		log.Fatal(err)
	}
}
