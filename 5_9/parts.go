package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	_ "image/jpeg"
	"log"
)

var (
	gopherImage *ebiten.Image
)

// ebiten.ImagePartsインターフェースを実装する構造体。
// 4つのパーツに分割して描画する
type fourParts struct {
	ebiten.ImageParts

	// 描画対象となる画像。
	// この構造体内ではサイズを取得するためだけに使う
	// image *ebiten.Image

	// 直接数値だけ与えてもよさげ
	w, h int
}

// 描画するパースの数を返す関数
func (f *fourParts) Len() int {
	return 4 // 4つのパースを描画することを表す
}

// 描画元の矩形の範囲を返す関数
func (f *fourParts) Src(i int) (int, int, int, int) {
	// Lenが4を返すので、iの範囲は0から3になる。
	// それぞれに対応した描画元矩形の範囲を返す。
	// それぞれは左上から右下に当てはまり、左上、右上、左下、右下のパーツに分解される
	switch i {
	case 0:
		return 0, 0, f.w / 2, f.h / 2
	case 1:
		return f.w / 2, 0, f.w, f.h / 2
	case 2:
		return 0, f.h / 2, f.w / 2, f.h
	case 3:
		return f.w / 2, f.h / 2, f.w, f.h
	default:
		panic("not reach")
	}
}

// 描画先の矩形の範囲を返す関数
func (f *fourParts) Dst(i int) (int, int, int, int) {
	// 少し隙間をあけて表示してみる
	const margin = 10
	switch i {
	case 0:
		return 0, 0, f.w / 2, f.h / 2
	case 1:
		return f.w/2 + margin, 0, f.w + margin, f.h / 2
	case 2:
		return 0, f.h/2 + margin, f.w / 2, f.h + margin
	case 3:
		return f.w/2 + margin, f.h/2 + margin, f.w + margin, f.h + margin
	default:
		panic("not reach")
	}
}

func update(screen *ebiten.Image) error {
	if ebiten.IsRunningSlowly() {
		return nil
	}

	w, h := gopherImage.Size()

	op := new(ebiten.DrawImageOptions)
	op.ImageParts = &fourParts{w: w, h: h}
	_ = screen.DrawImage(gopherImage, op)

	return nil
}

func main() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)

	var err error
	gopherImage, err = ebitenutil.NewImageFromURL("https://raw.githubusercontent.com/hajimehoshi/ebiten-book-code/master/ja/gophers_photo.jpg")
	if err != nil {
		log.Fatal(err)
	}

	if err := ebiten.Run(update, 320, 240, 2, "Parts"); err != nil {
		log.Fatal(err)
	}
}
