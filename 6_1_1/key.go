package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"log"
)

func update(screen *ebiten.Image) error {
	// 入力は描画ではなく論理更新の範疇らしいので、IsRunningSlowlyより前に処理する

	if !ebiten.IsKeyPressed(ebiten.KeyA) {
		return nil
	}

	if ebiten.IsRunningSlowly() {
		return nil
	}

	_ = ebitenutil.DebugPrint(screen, "A key is pressed!")

	return nil
}

func main() {
	if err := ebiten.Run(update, 320, 240, 2, "Key"); err != nil {
		log.Fatal(err)
	}
}
